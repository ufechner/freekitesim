Version 0.4
-----------

Simulation updates
~~~~~~~~~~~~~~~~~

- Updated the code for the use of numba 0.18.2 leading to an execution time decrease and sped up just-in-time compiling significantly
  by a factor of 2
- Reimplemented the core functions to not allocate memory for temporary arrays leading
  to an execution time decrease by a factor of 3, making the code 6 times faster in total
- Code enables the use of different WindProfileLaws
- Fixed a bug in the tether drag calculation which should be much more accurate now

Installer updates
~~~~~~~~~~~~~~~~~

- Next to the installation script the installation can be done by using a conda package
- Uses newest packages except for pandas and cgkit which need to be pinned with conda


Version 0.3
-----------

- Added logging of simulation data. The created logfiles can then be replayed
  with the kiteplayer.
- Increased simulation speed by using lookup tables instead of interpolation in
  every step. Current execution time of KPS4P.py on a Core i5-4670@3.4GHz is about 70μs, 
  30μs are needed for running the simulation in real-time.

Version 0.2
-----------

- Added installation script for Ubuntu 12.04, 64 bits. It should work on any modern 64 bit
  Linux installation.

Version 0.1
-----------

- Added fully working 4-point kite model with segmented tether, reel-out and depower capable.
- Complete installation instructions for manual installation on Ubuntu 12.04, 64 bits.
