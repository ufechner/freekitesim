#!/usr/bin/env python

from __future__ import absolute_import
import shutil
import subprocess
import sys
import zipfile
import getpass
from os.path import isfile, expanduser, expandvars, samefile
import os
from conda.fetch import fetch_index, fetch_pkg
from conda.install import extract, link
import json
import urllib

# TODO: allow to install more then one package with one command, e.g.
#       python fks_control.py install numpy scipy

CONFIG = ".kitepowerrc"
USER = expanduser("~")+'/'
KITE_ENV = USER+"00PythonSoftware/"
PYTHON = "http://repo.continuum.io/pkgs/free/linux-64/python-2.7.9-2.tar.bz2"
REQUESTS = "http://repo.continuum.io/pkgs/free/linux-64/requests-2.6.0-py27_0.tar.bz2"
OPENSSL = "http://repo.continuum.io/pkgs/free/linux-64/openssl-1.0.1k-0.tar.bz2"
ASSIMULO = "https://dl.dropboxusercontent.com/u/9075004/assimulo-2.5-py27_0.tar.bz2.zip"
USAGE = """
Usage: python fks_control.py command [argumment]
  commands
    install-base:       Create the basic working environment
    install package:    Install package
    remove [package]:   Remove the environment or only package if given
    info:               Print this help message
    list:               Lists the installed packages
"""


def download(link):
    urllib.urlretrieve (link, KITE_ENV+'pkgs/'+link.split('/')[-1])

def log(message):
    print message

def logwarning(message):
    print
    print '\033[93mERR: ' + message + '\033[0m'
    print

def logerr(message):
    print
    print '\033[91mERR: ' + message + '\033[0m'
    print
    sys.exit()

def gitClone(repo_link, repoDir):
    cmd = ['git', 'clone', repo_link]
    p = subprocess.Popen(cmd, cwd=repoDir)
    p.wait()

def prompt_true(message):
    while True:
        ans = raw_input(message + " [y]/n ")
        if ans == '':
            return True
        elif ans.lstrip()[0] in 'yY':
            return True
        elif ans.lstrip()[0] in 'nN':
            return False
        else:
            print "Please answer either yes or no"
            continue

def prompt_false(message):
    while True:
        ans = raw_input(message + " y/[n] ")
        if ans == '':
            return False
        elif ans.lstrip()[0] in 'yY':
            return True
        elif ans.lstrip()[0] in 'nN':
            return False
        else:
            print "Please answer either yes or no"
            continue

class KitepowerPackageManager:

    def __init__(self):
        # Assess current state of the installation
        config_exists = isfile(USER+CONFIG)
        self.fks_exists = False
        if len(sys.argv) == 1:
            print USAGE
            sys.exit()

        if sys.argv[1] == 'install-base':
            self.install_base()
            sys.exit()

        if config_exists and not os.stat(USER+CONFIG)[6]==0:
            with open(USER+CONFIG) as conffile:
                self.config = json.load(conffile)
        else:
            prompt_true("No base installation detected. Create it now?")
            self.install_base()

        # Create empty structure for dependency management
        self.temp_strict = []
        self.cache = {}

        # Check dependencies

        # TODO: Check if newer versions of packages are available and ask if
        # shall be updated

        if sys.argv[1] == 'install':
            if not len(sys.argv) > 2:
                logerr(USAGE)
            self.install(sys.argv[2])
        elif sys.argv[1] == 'remove':
            if not len(sys.argv) > 2:
                if not prompt_false("Are you sure you want to remove the whole environment?"):
                    sys.exit()
                shutil.rmtree(KITE_ENV+'bin')
                shutil.rmtree(KITE_ENV+'lib')
                shutil.rmtree(KITE_ENV+'include')
                shutil.rmtree(KITE_ENV+'pkgs')
                shutil.rmtree(KITE_ENV+'share')
                shutil.rmtree(KITE_ENV+'ssl')
                shutil.rmtree(KITE_ENV+'conda-meta')
                shutil.rmtree(KITE_ENV+'plugins')
                os.remove(USER+'.kitepowerrc')
                if not prompt_false("Would you like to remove the FreeKiteSim repository as well?"):
                    sys.exit()
                shutil.rmtree(KITE_ENV+'FreeKiteSim')
            self.remove(sys.argv[2])
        elif sys.argv[1] == 'info':
            self.info()
        elif sys.argv[1] == 'search' and len(sys.argv)>2:
            self.search(sys.argv[2], True)
        elif sys.argv[1] == 'list':
            self.list_installed()

    def info(self):
        print USAGE
        sys.exit()

    def install_base(self):
        """""
        Installs the base environment if it does not exist yet
        """
        print "Installing base system"

        # path = raw_input("Path to Environment(if it does not exist yet install path)?\n" + \
        #                  "(default[ENTER] $HOME/00PythonSoftware) ")
        path = ''
        if path == '':
            path = KITE_ENV

        path = expanduser(expandvars(path))
        # Make sure that the last character is a slash such that file names can
        # easily be appended to the path name
        if not path[-1] == '/':
            path = path + '/'

        try:
            os.makedirs(path)
        except OSError as err:
            if err.errno != 17:
                raise
        try:
            # Create neccessary environment folders
            os.mkdir(path+"/bin")
            os.mkdir(path+"/lib")
            os.mkdir(path+"/include")
            os.mkdir(path+"pkgs")
        except OSError as err:
            if err.errno != 17:
                raise

        open(path+"/.package_index", 'a').close()
        self.config = {}
        self.config['home'] = path
        self.config['installed'] = ['python-2.7.9-2.tar.bz2', 'requests-2.6.0-py27_0.tar.bz2', 'openssl-1.0.1k-0.tar.bz2']
        self.config['channels'] = ["http://conda.binstar.org/ufechner/linux-64/",
                "http://conda.binstar.org/janharms/linux-64/",
                "http://conda.binstar.org/mutirri/linux-64/",
                "http://repo.continuum.io/pkgs/free/linux-64/",
                "http://conda.binstar.org/tlatorre/linux-64/"]
        self.config['software'] = KITE_ENV
        self.config['strict'] = [[["python", "2.7*"], ["requests-2.6.0-py27_0.tar.bz2"], ["openssl-1.0.1k-0.tar.bz2"]]]
        self.config['larger'] = []
        self.config['depends'] = {}

        # Check status of installed Software
        ls = os.listdir(self.config['software'])
        if "FreeKiteSim" in ls:
            self.fks_exists = True

        # Make sure that freeglut3 and gfortran are installed
        try:
            import apt
            cache = apt.Cache()
            fg3 = False
            gf = False
            if cache['freeglut3'].is_installed:
                fg3 = True
            if cache['gfortran'].is_installed:
                gf = True
            if not fg3 or not gf:
                logerr("Make sure that freeglut3 and gfortran are installed." + \
                       "Run: sudo apt-get install freeglut3 gfortran")
        except ImportError:
            if not prompt_false("FreeKiteSim requires freeglut3 and gfortran.\n" + \
                                "Please make sure these packages are installed before continuing\n" + \
                                "with the installation. Continue?"):
                sys.exit()

        # if not self.fks_exists:
        # try:
                # gitClone('https://bitbucket.org/ufechner/FreeKiteSim.git', KITE_ENV)
        # except OSError:
        # logerr("""git seems not to be installed. Please install it. For Ubunutu run:\n\tsudo apt-get install git""")

        # Install necessary dependencies
        download(PYTHON)
        download(REQUESTS)
	download(OPENSSL)
        extract(self.config['home']+"pkgs/", PYTHON.split('/')[-1].rsplit('.', 2)[0])
        link(self.config['home']+"pkgs/", self.config['home'], PYTHON.split('/')[-1].rsplit('.', 2)[0])
        os.remove(self.config['home']+"pkgs/"+PYTHON.split('/')[-1])
        extract(self.config['home']+"pkgs/", REQUESTS.split('/')[-1].rsplit('.', 2)[0])
        link(self.config['home']+"pkgs/", self.config['home'], REQUESTS.split('/')[-1].rsplit('.', 2)[0])
        os.remove(self.config['home']+"pkgs/"+REQUESTS.split('/')[-1])
        extract(self.config['home']+"pkgs/", OPENSSL.split('/')[-1].rsplit('.', 2)[0])
        link(self.config['home']+"pkgs/", self.config['home'], OPENSSL.split('/')[-1].rsplit('.', 2)[0])
        os.remove(self.config['home']+"pkgs/"+OPENSSL.split('/')[-1])
        sys.path.append(KITE_ENV+"lib/python2.7/site-packages/")
        os.environ["PATH"] = (KITE_ENV+"bin:")+os.environ["PATH"]

        self.install('freekitesim')

        # remove libm files which is in conflict with system libm
        # TODO: remove this conflict, installed by system package
        os.rename(self.config['home']+"lib/libm.so.6", self.config['home']+"lib/libm.so.6.back")
        os.rename(self.config['home']+"lib/libm.so", self.config['home']+"lib/libm.so.back")

        # Add new Environment to path
        log("To enable the environment run source ~/.bashrc or restart bash.")

        # Append path to ~/.bashrc
        with open(USER+".bashrc", 'a') as bashconf:
            bashconf.write("\n# Added by freekitesim installation\nexport PATH="+self.config['home']+"bin:$PATH")

        with open(USER+CONFIG, 'w') as outfile:
            json.dump(self.config, outfile, indent=4, separators=(',', ': '))

    def install_assimulo(self):

        log("Installing Assimulo ...")
        pwd = getpass.getpass("Password for unziping assimulo")
        z = zipfile.ZipFile(self.config['home']+"pkgs/"+ASSIMULO.split('/')[-1])
        z.extractall(path=self.config['home']+"pkgs/", pwd=pwd)
        z.close()
        extract(self.config['home']+"pkgs/", ASSIMULO.split('/')[-1].rsplit('.', 3)[0])
        link(self.config['home']+"pkgs/", self.config['home'], ASSIMULO.split('/')[-1].rsplit('.', 3)[0])
        os.remove(self.config['home']+"pkgs/"+ASSIMULO.split('/')[-1])
        os.remove(self.config['home']+"pkgs/"+ASSIMULO.split('/')[-1].rsplit('.', 1)[0])


    def check_bashrc(self):
        """""
        Checks whether the the .bashrc file of the user has an entry for the
        environment and returns:
        0 - if no entry is found
        1 - if entry is found but commented (deactivated)
        2 - if entry is found and not commented (activated)
        """
        with open(USER+"/.bashrc", 'a+') as bashrc:
            lines = bashrc.readlines()
        for i, line in enumerate(lines):

            if "export" in line and "PATH" in line:
                try:
                    path = line.split('=')[1].split(':')[0].strip(' \'\"')
                except IndexError:
                    pass
                if samefile(expanduser(path), self.config['home']) or samefile(expandvars(path), self.config['home']):
                    if line.lstrip()[0] == '#':
                        return (1, i+1)
                    return (2, i+1)
        return (0, -1)


    # TODO: make it possible to install a specific version of a package
    def install(self, package):
        """
        Installs the package and all its dependencies into the environment.
        """

        print "Fetching index ..."
        self.cache = fetch_index(tuple(self.config['channels']))

        tbi = self.resolve(package)

        if not tbi:
            sys.exit()

        log("The following packges will be installed: \n")

        tbi = sorted(tbi)

        # List the packages for the user
        print '\t{0:20}{1}'.format('name', 'version')
        print '\n'.join('\t{:20}{}'.format(*(i.rsplit('-', 2))) for i in tbi)

        if not prompt_true("\nWould you like to continue with the installation?"):
            sys.exit()
            return

        # TODO: make a more accurate progress bar
        # first download all packages
        for pkg_name in tbi:
            print '\rDownloading {0}'.format(pkg_name),
            fetch_pkg(self.cache[pkg_name], self.config['home']+"pkgs/")
        print

        i = 0
        for pkg_name in tbi:
            print 'Installing package {0}\r'.format(pkg_name),
            extract(self.config['home']+"pkgs/", pkg_name.rsplit('.', 2)[0])
            link(self.config['home']+"pkgs/", self.config['home'], pkg_name.rsplit('.', 2)[0])
            os.remove(self.config['home']+"pkgs/"+pkg_name)
            i += 1
        print

        # Add the new installation to the config file
        self.config['strict'] = self.temp_strict
        self.config['larger'] = self.temp_larger
        self.config['installed'].extend(tbi);
        self.config['depends'] = self.temp_depends

        with open(USER+CONFIG, 'w') as outfile:
            json.dump(self.config, outfile, indent=4, separators=(',', ': '))

        self.temp_strict = []
        self.temp_larger = []

        print "Installation succesful"+50*" "

    def resolve(self, package):
        """Reolves the dependencies to check for conflicts.
        Returns a list of packages that need to be installed."""

        packages = [package]
        self.temp_strict = self.config['strict']
        self.temp_larger = self.config['larger']
        self.temp_depends = self.config['depends']
        i = 0
        j = 0
        tbi = []
        previous = []
        prev_conf = []

        print "Resolving dependencies ..."
        while j < len(packages):
            con = False
            matches = self.search(packages[j])

            if not matches:
                logerr("The package %s could not be found. Please check your channels settings" % packages[j])
                return None

            matches = sorted(matches.items(), key=lambda x: x[0], reverse=True)

            if prev_conf:
                if i == len(matches):
                    logerr("Dependencies could not be resolved. Conflict with package %s %s"%(prev_conf[0], prev_conf[1]))

                while not any([True for x in matches[i][1]['depends'] \
                          if prev_conf[0] in x and not prev_conf[1].replace('*', '') in x]):
                    if len(matches) > i+1:
                        i += 1
                    else:
                        logerr("Dependencies could not be resolved!")

            # If there is a strict requirement for this package,
            # search until a proper version was found
            # TODO: If no more matches can be found go further back in the history
            for x in self.temp_strict:
                while packages[j] == x[0][0] and not x[0][1].replace('*', '') in matches[i][1]['version']:
                    if len(matches) > i+1:
                        i += 1
                    else:
                        i = 0
                        try:
                            j = tbi.index(x[1][0])
                        except ValueError:
                            self.remove(x[1][0])
                            continue
                        prev_conf = [x[0][0].rsplit('-', 2)[0], x[0][1]]
                        self.revert(tbi, j)
                        con = True
            if con:
                continue

            # Resolve dependencies
            result, pkg_conflict = self.conflict(matches[i], packages, tbi)

            # In case of an error we have to try an older package and start
            # again
            if not result and pkg_conflict:
                # First try looping through the existing matches
                if len(matches) > i+1:
                    i += 1
                # Go back to the package with the conflicting dependency and
                # remove all packages from the To-be-installed list
                else:
                    i = 0
                    prev_conf = pkg_conflict
                    for x in self.temp_strict:
                        if prev_conf[0] == x[0][0].rsplit('-', 2)[0]:
                            previous = x[1][0]
                    j = tbi.index(previous)
                    self.revert(tbi, j)

            else:
                packages.extend(result)
                if not matches[i][0] in tbi:
                    tbi.append(matches[i][0])

                # Add dependencies of new package to dict
                for dep in matches[i][1]['depends']:
                    self.temp_depends[dep.split(' ')[0]] = self.temp_depends.get(dep.split(' ')[0], []) + [matches[i][0]]

                i = 0
                j += 1
                previous = []
                prev_conf = []

        return tbi

    def revert(self, tbi, j):
        for pac in tbi[j:]:
            for l, x in enumerate(self.temp_strict):
                for k, dep in enumerate(x[1]):
                    if dep == pac:
                        x[1][k] = ""
        for pac in tbi[j:]:
            for l, x in enumerate(self.temp_larger):
                for k, dep in enumerate(x[1]):
                    if dep == pac:
                        x[1][k] = ''
        for pac in tbi[j:]:
            for key, val in self.temp_depends.iteritems():
                m = 0
                while m < len(val):
                    if pac == val[m]:
                        del val[m]
                        continue
                    m += 1
        self.temp_depends = dict((k,v) for k, v in self.temp_depends.iteritems() if v)

        # Remove the previously created empty strings
        self.temp_strict = [x for x in [[x[0], filter(None, x[1])] for x in self.temp_strict] if x[1]]
        self.temp_larger = [x for x in [[x[0], filter(None, x[1])] for x in self.temp_larger] if x[1]]

        del tbi[j:]

    def conflict(self, package, packages, tbi):
        """
        Checks if the dependencies can be met.
        """
        exist = self.config['installed']
        strict_exist = self.temp_strict
        larger_exist = self.temp_larger
        depends = [x for x in package[1]['depends']]
        strict = [x.split(' ') for x in depends if ' ' in x and not '>' in x]
        larger = [x.split(' >') for x in depends if '>' in x]
        depends = [x.split(' ') for x in depends]

        # Check for strict requirements
        for n in strict:
            for e in strict_exist:
                if n[0] == e[0][0] and not n[1].replace("*", "") == e[0][1].replace("*", ""):
                    return ([], n)
            for e in larger_exist:
                if n[0] == e[0][0] and n[1].replace("*", "") < e[0][1].replace("*", ""):
                    return ([], n)
        for n in larger:
            for e in strict_exist:
                if n[0] == e[0][0] and e[0][1].replace("*", "") < n[1].replace("*", ""):
                    return ([], n)

        for n in strict:
            for e in tbi:
                if e.rsplit('-', 2) == n[0] and not n[1].resplace("*", "") == e.rsplit('-',1)[0].rsplit('-', 1)[1]:
                    #TODO: remove the package from tbi list and append the new one
                    pass

        # Find what new dependecies to add
        new = []
        if not len(exist) == 0:
            for x in depends:
                if not x[0] in [y.rsplit('-', 2)[0] for y in exist] and not x[0] in packages:
                    new.append(x[0])
        else:
            new = [x[0] for x in depends if not x[0] in packages]

        for x in strict:
            extended = False
            for y in self.temp_strict:
                if x == y[0] and not package[0] in y[1]:
                    y[1].append(package[0])
                    extended = True
            if not extended:
                self.temp_strict.extend([[x, [package[0]]]])
        for x in larger:
            extended = False
            for y in self.temp_larger:
                if y[0] == x and not [package[0] in y[1]]:
                    y[1].append(package[0])
                    extended = True
            if not extended:
                self.temp_larger.extend([[x, [package[0]]]])

        # print "temp_strict, ", self.temp_strict
        # print "strict, ", strict

        return (new, None)


    def search(self, package_name, verbose=False):
        """""
        Search the index for all available packages
        """
        if not self.cache:
            self.cache = fetch_index(tuple(self.config['channels']))

        matches = {k:v for k, v in self.cache.iteritems() if package_name == v['name']}
        sorted_matches = sorted(matches.items(), key=lambda x: x[0], reverse=True)

        if verbose:
            print
            if matches:
                print "Matches found for %s" % package_name
                print '{0:20}{1}'.format('name', 'version')
            else:
                print 'No matches found for %s' % package_name
            for k in sorted_matches:
                # print v.keys(), v.values()
                print '{0:20}{1}'.format(k[1]['name'], k[1]['version'])
        return matches

    def remove(self, package, autoremove=False):
        """""
        Remove the package specified by package
        """

        if not self.temp_strict:
            self.temp_strict = self.config['strict']
            self.temp_larger = self.config['larger']
            self.exist = self.config['installed']
            self.temp_depends = self.config['depends']

        tbr = []
        # Get the exact package description
        print package
        print self.exist
        for pac in self.exist:
            if package == pac.rsplit('-', 2)[0]:
                tbr.append(pac)

        i = 0
        while i < len(tbr):
            for key, val in self.temp_depends.iteritems():
                if key == tbr[i].rsplit('-', 2)[0]:
                    tbr.extend([v for v in val])
                    val = []

                m = 0
                while m < len(val):
                    if tbr[i] == val[m]:
                        del val[m]
                        continue
                    m += 1

            # Check if any other package depends on this package and ask if it
            # should be removed as well (autoremove)
            if autoremove:
                auto_packages = [k for k, v in self.temp_depends.iteritems() if not v]
                for k in auto_packages:
                    for x in self.exist:
                        if k == x.rsplit('-', 2)[0]:
                            tbr.append(x)

            self.temp_depends = dict((k, v) for k, v in self.temp_depends.iteritems() if v)

            i += 1

        print "The following packages will be removed:"
        if tbr == []:
            print "No package found by the name of %s" % package
            sys.exit()
            return
        print "{0:20}{1:10}".format("name", "version")
        for pac in tbr:
            print "{0:20}{1:10}".format(pac.rsplit('-', 2)[0], pac.rsplit('-', 1)[0].rsplit('-', 1)[1])

        if not prompt_false("\nWould you like to continue with removal?"):
            sys.exit()
            return

        # At this point a list of packages that need to be removed should exist
        # For each package, get all files that were installed by the package and
        # remove them from the system.
        for pac in tbr:
            with open(self.config['home']+"conda-meta/"+pac.rsplit('.', 2)[0]+".json") as metafile:
                meta = json.load(metafile)
                for f in meta['files']:
                    os.remove(self.config['home']+f)
            os.remove(self.config['home']+"conda-meta/"+pac.rsplit('.',
                2)[0]+".json")

            # Remove the entries in the strict and larger dependencies
        for dep in self.temp_strict:
            for pac in tbr:
                if dep[0][0] == pac.rsplit('-', 2)[0]:
                    del dep
                else:
                    dep[1] = [x for x in dep[1] if not x == pac]

        for dep in self.temp_larger:
            for pac in tbr:
                if dep[0][0] == pac.rsplit('-', 2)[0]:
                    del dep
                else:
                    dep[1] = [x for x in dep[1] if not x == pac]

        # Write changes to config file
        self.config['strict'] = self.temp_strict
        self.config['larger'] = self.temp_larger
        for pac in tbr:
            self.config['installed'].remove(pac)
        self.config['depends'] = self.temp_depends

        with open(USER+CONFIG, 'w') as outfile:
            json.dump(self.config, outfile, indent=4, separators=(',', ': '))


    def list_installed(self):

        print "{0:20}{1:10}".format("name", "version")
        for pac in self.config["installed"]:
            print "{0:20}{1:10}".format(pac.rsplit('-', 2)[0], pac.rsplit('-', 1)[0].rsplit('-', 1)[1])


if __name__ == "__main__":
    k = KitepowerPackageManager()
