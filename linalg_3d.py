# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
""" Fast implementation of the following linear algebra functions for 3-dimensional arrays:
    cross, dot, norm, normalize.
    Average speed-up factor compared to numpy around 8 to 9, heavily machine dependant,
    but also dependent on the call context: If these procedures are called from another
    procedure, that was also compiled with Numba they can be much faster than when called
    from standard Python code. 
    Requires the library "numba" version 0.11.1 !!!  """
# TODO: add test case for dot with 3x3 rotation matrix
# pylint: disable=E0611
from numba import autojit, jit, double
import math
import numpy as np
import numpy.linalg as la
from Timer import Timer
from cgkit.cgtypes import vec3

# Fails with numba 0.12
@autojit
def cross(vec1, vec2):
    """ Calculate the dot product of two 3d vectors. """
    result = np.zeros(3)
    a1, a2, a3 = double(vec1[0]), double(vec1[1]), double(vec1[2])
    b1, b2, b3 = double(vec2[0]), double(vec2[1]), double(vec2[2])
    result[0] = a2 * b3 - a3 * b2
    result[1] = a3 * b1 - a1 * b3
    result[2] = a1 * b2 - a2 * b1
    return result

# OK
@autojit(nopython=True)
def dot(vec1, vec2):
    """ Calculate the dot product of two 3d vectors. """
    return vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2]

# OK
@autojit(nopython=True)
def limit(number):
    if number > 1.0:
        return 1.0
    if number < -1.0:
        return -1.0
    return number

# not needed
def cg_dot(vec1, vec2):
    """ Calculate the dot product of two 3d vectors. """
    return vec1 * vec2

# OK
@autojit(nopython=True)
def norm(vec):
    """ Calculate the norm of a 3d vector. """
    return math.sqrt(vec[0]**2 + vec[1]**2 + vec[2]**2)

# not needed
def cg_norm(vec):
    return vec.length()

# OK
@autojit(nopython=True)
def ground_dist(vec):
    """ Calculate the ground distance from the kite position (x,y,z, z up). """
    return math.sqrt(vec[0]**2 + vec[1]**2)

# OK
@autojit(nopython=True)
def calc_elevation(vec):
    """ Calculate the elevation angle in radian from the kite position """
    return math.atan(vec[2]/ground_dist(vec))

# Fails with numba 0.12
@autojit(nopython = True)
def azimuth_east(vec):
    """ Calculate the azimuth angle in radian from the kite position in ENU reference frame.
    Zero east. Positive direction clockwise seen from above.
    Valid range: -pi .. pi."""
    return -math.atan2(vec[1], vec[0])

# Fails with numba 0.12
@autojit(nopython = True)
def azimuth_north(vec):
    result = azimuth_east(vec)
    result += math.pi/2.0
    if result > math.pi:
        result -= 2.0 * math.pi
    return result

# OK
@autojit()
def normalize(vec):
    """ Calculate the normalized vector (norm: one). """
    norm_ = norm(vec)
    if norm_ < 1e-6:
        result = np.zeros(3)
    else:
        return vec / norm_
    return result

# OK
def numpy_normalize(vec):
    """ Calculate the normalized vector (norm: one). """
    return vec / la.norm(vec)

# OK
@autojit()
def calc_alpha(v_app, vec_z):
    """
    Calculate the angle of attack alpha from the apparend wind velocity vector
    v_app and the z unit vector of the kite reference frame. """
    return math.pi/2.0 - math.acos(dot(v_app, -vec_z) / norm(v_app))

def calc_alpha_rolf(v_app, vec_z):
    """
    Calculate the angle of attack alpha from the apparend wind velocity vector
    v_app and the z unit vector of the kite reference frame. """
    return  math.acos(dot(v_app, vec_z) / norm(v_app)) - math.pi/2.0

def init():
    """ call all functions once to compile them """
    vec1, vec2 = np.array((1.0, 2.0, 3.0)), np.array((2.0, 3.0, 4.0))
    cross(vec1, vec2)
    dot(vec1, vec2)
    norm(vec1)
    ground_dist(vec1)
    calc_elevation(vec1)
    azimuth_east(vec1)
    vec_z = normalize(vec2)
    calc_alpha(vec1, vec_z)
    unit_vector = normalize(vec1)
    unit_vector *= 2 # suppress warning unused variable
    limit(2.0)

init()

if __name__ == '__main__':
    vec1 = np.array((1.0, 2.0, 3.0))
    vec2 = np.array((2.0, 3.0, 4.0))
    orient = np.array((0, math.pi, math.pi / 2.0))
    assert dot(vec1, vec2) == np.dot(vec1, vec2)
    assert (cross(vec1, vec2) == np.cross(vec1, vec2)).all()
    assert ground_dist(vec1) == math.sqrt(5)
    assert calc_elevation(vec1) == math.atan(3.0/math.sqrt(5))
    assert np.array_equal(normalize(vec1), numpy_normalize(vec1))
    print "azimuth_east: ", azimuth_east(vec1) * 180.0 / math.pi
    v_app = np.array((10.0, 2.0, 3.0))
    vec_z = normalize(np.array((3.0, 2.0, .0)))
    alpha = calc_alpha(v_app, vec_z)
    #alpha2 = calc_alpha2(v_app, vec_z)
    alpha_rolf = calc_alpha_rolf(v_app, vec_z)
    print 'alpha, alpha_rolf', alpha, alpha_rolf
    print 'alpha: ', alpha * 180.0 / math.pi
    print 'limit(2.0)', limit(2.0)
    print 'limit(-2.0)', limit(-2.0)
    with Timer() as t0:
        for i in range(10000):
            pass
    print "time for empty loop ", t0.secs
    print
    with Timer() as t1:
        for i in range(10000):
            norm(vec1)
    print "time for numba norm  [µs]:   ", (t1.secs-t0.secs)  / 10000 * 1e6
    with Timer() as t2:
        for i in range(10000):
            la.norm(vec1)
    print "time for linalg norm [µs]:   ", (t2.secs - t0.secs)  / 10000 * 1e6
    with Timer() as t9:
        cgvec1 = vec3(vec1)
        for i in range(10000):
            cg_norm(cgvec1)
    print "time for cgkit norm  [µs]:   ", (t9.secs - t0.secs)  / 10000 * 1e6
    print "speedup of norm with numba: ", (t2.secs - t0.secs)  / (t1.secs - t0.secs)
    print
    with Timer() as t3:
        for i in range(10000):
            dot(vec1, vec2)
    print "time for numba dot [µs]:      ", (t3.secs - t0.secs) / 10000 * 1e6
    with Timer() as t4:
        for i in range(10000):
            np.dot(vec1, vec2)

    print "time for numpy dot [µs]:      ", (t4.secs - t0.secs) / 10000 * 1e6
    with Timer() as t10:
        cgvec2 = vec3(vec2)
        for i in range(10000):
            cg_dot(cgvec1, cgvec2)
    print "time for cgkit dot  [µs]:     ", (t10.secs - t0.secs)  / 10000 * 1e6
    print "speedup of dot with numba:    ", (t4.secs - t0.secs) / (t3.secs - t0.secs)
    print
    with Timer() as t5:
        for i in range(10000):
            cross(vec1, vec2)
    print "time for numba cross [µs]:    ", (t5.secs - t0.secs) / 10000 * 1e6
    with Timer() as t6:
        for i in range(10000):
            np.cross(vec1, vec2)
    print "time for numpy cross [µs]:    ", (t6.secs - t0.secs) / 10000 * 1e6
    print "speedup of cross with numba:  ", (t6.secs - t0.secs) / (t5.secs - t0.secs)
    print
    with Timer() as t7:
        for i in range(10000):
            azimuth_east(vec1)
    print "time for azimuth_east [µs]:   ", (t7.secs - t0.secs) / 10000 * 1e6
    z = normalize(vec2)
    with Timer() as t8:
        for i in range(10000):
            calc_alpha(vec1, z)
    print "time for calc_alpha [µs]:     ", (t8.secs - t0.secs) / 10000 * 1e6
    with Timer() as t9:
        for i in range(10000):
            z = normalize(vec2)
    print "time for numba normalize [µs]:", (t9.secs - t0.secs) / 10000 * 1e6
    with Timer() as t9:
        for i in range(10000):
            z = numpy_normalize(vec2)
    print "time for numpy normalize [µs]:", (t9.secs - t0.secs) / 10000 * 1e6

"""
Result on Core duo, 2 Ghz:

time for empty loop  0.000799894332886

time for numba norm [µs]:      1.14262104034
time for linalg norm [µs]:    20.1265096664
speedup of norm with numba:   17.6143348983

time for numba dot [µs]:       1.12090110779
time for numpy dot [µs]:       1.58061981201
speedup of dot with numba:     1.41013315183

time for numba cross [µs]:     3.1898021698
time for numpy cross [µs]:    26.0297060013
speedup of cross with numba:   8.16028851185
"""

"""
Results on i7-3770 CPU @ 3.40GHz

time for empty loop  0.000322103500366

time for numba norm [µs]:      0.347185134888
time for linalg norm [µs]:    7.63418674469
speedup of norm with numba:   21.9888064826

time for numba dot [µs]:       0.433993339539
time for numpy dot [µs]:       0.725889205933
speedup of dot with numba:     1.67258144262

time for numba cross [µs]:     1.3659954071
time for numpy cross [µs]:    9.7697019577
speedup of cross with numba:   7.15207526093
"""
"""
Results on Tuxedo laptop with i7-4800 MQ CPU:

time for empty loop  0.000317811965942

time for numba norm  [µs]:    0.351905822754
time for linalg norm [µs]:    4.31990623474
time for cgkit norm  [µs]:    0.353121757507
speedup of norm with numba:  12.2757452575

time for numba dot [µs]:       0.45371055603
time for numpy dot [µs]:       0.783109664917
time for cgkit dot  [µs]:      0.523209571838
speedup of dot with numba:     1.72601156069

time for numba cross [µs]:     1.0849237442
time for numpy cross [µs]:     5.54261207581
speedup of cross with numba:   5.10875727942

time for azimuth_east [µs]:    0.434112548828
time for calc_alpha [µs]:      1.30860805511
time for numba normalize [µs]: 0.566220283508
time for numpy normalize [µs]: 6.40771389008
"""
