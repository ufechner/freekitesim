# -*- coding: utf-8 -*-
"""
This is a test script for the module KPS4P_v2.py. The following functions are tested:
calcParticleForces, innerLoop2, calcAeroForces4 . More tests needed.
"""
import numpy as np
import numpy.linalg as la
import math
from math import acos, pi, radians
from scipy.interpolate import InterpolatedUnivariateSpline
from Settings import SPRINGS, SEGMENTS, PARTICLES, D_TETHER, ELEVATION, KITE_PARTICLES, L_0, C_SPRING, \
                     AREA, ALPHA_ZTIP, REL_SIDE_AREA, MASS, KCU_MASS, MASSES, WINCH_MODEL, DAMPING
from KPS4P_v2 import calcParticleForces, innerLoop2, calcAeroForces4, loop, Alpha_zero, D_tether, Alpha, \
                     V_wind_gnd, V_wind_tether, V_wind, Length, C_spring, Stiffnes_factor, Damping, SCALARS_SIZE, \
                     VEC3_SIZE
from Approximate_CLCD import approximate_cl, approximate_cd
# pylint: disable=E1101

# TODO: Add test for for the function loop

G_EARTH      = np.array([0.0, 0.0, -9.81])
ALPHA = 1.0/7.0
K_D = 1.0
C_D_TETHER = 0.958
RHO_0 = 1.225 # kg / m³

KD = 40.0 / 180.0 * math.pi # max depower
KS = radians(16.565 * 1.064 * 0.875 * 1.033 * 0.9757 * 1.083)  # max steering
K = 1 - REL_SIDE_AREA # correction factor for the drag
DRAG_CORR = 0.93       # correction of the drag for the 4-point model
NO_PARTICLES = SEGMENTS + KITE_PARTICLES + 1

""" Calculate the aerodynamic kite properties. """
# pylint: disable=C0326
ALPHA_CL = [-180.0, -160.0, -90.0, -20.0, -10.0,  -5.0,  0.0,   20.0, 40.0, 90.0, 160.0, 180.0]
CL_LIST  = [   0.0,    0.5,   0.0,  0.08, 0.125,  0.15,  0.2,1.0*K_D,  1.0,  0.0,  -0.5,   0.0]

ALPHA_CD = [-180.0, -170.0, -140.0, -90.0, -20.0, 0.0, 20.0,     90.0, 140.0, 170.0, 180.0]
CD_LIST  = [   0.5,    0.5,    0.5,   1.0,   0.2, 0.1,  0.2*K_D,  1.0,   0.5,   0.5,   0.5]
# pylint: enable=C0326

calc_cl = InterpolatedUnivariateSpline(ALPHA_CL, CL_LIST)
calc_cd = InterpolatedUnivariateSpline(ALPHA_CD, CD_LIST)

if False:
    calc_cl = InterpolatedUnivariateSpline(ALPHA_CL, CL_LIST)
    calc_cd = InterpolatedUnivariateSpline(ALPHA_CD, CD_LIST)
else:
    calc_cl = approximate_cl
    calc_cd = approximate_cd

def normalize(vec):
    """ Calculate the normalized vector (norm: one). """
    return vec / la.norm(vec)

def calcWindFactor(height):
    """ Calculate the relative wind speed at a given height and 6 m reference height. """
    return (height / 6.0)**ALPHA

def calcWindHeight(v_wind_gnd, height):
    """ Calculate the wind speed at a given height with 6 m reference height. """
    return v_wind_gnd * calcWindFactor(height)

def init():
    """ Calculate the initial conditions y0, yd0 and sw0. Tether with the given elevation angle,
        particel zero fixed at origin. """
    DELTA = 1e-6
    # self.setCL_CD(10.0 / 180.0 * math.pi)
    pos, vel, acc = [], [], []
    state_y = DELTA
    vel_incr = 0
    sin_el, cos_el = math.sin(ELEVATION / 180.0 * math.pi), math.cos(ELEVATION / 180.0 * math.pi)
    for i in range(SEGMENTS + 1):
        radius = - i * L_0
        if i == 0:
            pos.append(np.array([-cos_el * radius, DELTA, -sin_el * radius]))
            vel.append(np.array([DELTA, DELTA, DELTA]))
        else:
            pos.append(np.array([-cos_el * radius, state_y, -sin_el * radius]))
            if i < SEGMENTS:
                vel.append(np.array([DELTA, DELTA, -sin_el * vel_incr*i]))
            else:
                vel.append(np.array([DELTA, DELTA, -sin_el * vel_incr*(i-1.0)]))
        acc.append(np.array([DELTA, DELTA, -9.81]))
    pos_array, vel_array = np.array(pos, order='C'), np.array(vel, order='C')

    # kite particles (pos and vel)
    for i in range(KITE_PARTICLES):
        pos_array = np.vstack((pos_array, (PARTICLES[i+2]))) # Initial state vector
    # kite particles (vel and acc)
    for i in range(KITE_PARTICLES):
        vel_array = np.vstack((vel_array, (np.array([DELTA, DELTA, DELTA])))) # Initial state vector
    return pos_array, vel_array


def calcParticleForces_(pos1, pos2, vel1, vel2, v_wind_tether, spring, forces, stiffnes_factor, segments, \
                       d_tether, rho, i):
    """ Calculate the drag force of the tether segment, defined by the parameters po1, po2, vel1 and vel2
    and distribute it equally on the two particles, that are attached to the segment.
    The result is stored in the array self.forces.
    Reference implementation, using numpy (slow). """
    p_1 = int(spring[0])     # Index of point nr. 1
    p_2 = int(spring[1])     # Index of point nr. 2
    l_0 = spring[2]     # Unstressed length
    k = spring[3] * stiffnes_factor       # Spring constant
    # print "---->", k, spring[3], stiffnes_factor
    c = spring[4]       # Damping coefficient
    #print 'k, c: ', k, c
    segment = pos1 - pos2
    rel_vel = vel1 - vel2
    av_vel = 0.5 * (vel1 + vel2)
    norm1 = la.norm(segment)
    unit_vector = segment / norm1
    k1 = 0.25 * k # compression stiffness kite segments
    k2 = 0.1 * k # compression stiffness tether segments
    c1 = 6.0 * c  # damping kite segments
    # look at: http://en.wikipedia.org/wiki/Vector_projection
    # calculate the relative velocity in the direction of the spring (=segment)
    spring_vel = np.dot(unit_vector, rel_vel)
    if (norm1 - l_0) > 0.0:
        if i >= segments:  # kite springs
             spring_force = (k *  (norm1 - l_0) + (c1 * spring_vel)) * unit_vector
        else:
             spring_force = (k *  (norm1 - l_0) + (c * spring_vel)) * unit_vector
    elif i >= segments: # kite spring
        spring_force = (k1 *  (norm1 - l_0) + (c * spring_vel)) * unit_vector
    else:
        spring_force = (k2 *  (norm1 - l_0) + (c * spring_vel)) * unit_vector
    # print "spring_force: ", spring_force
    # Aerodynamic damping for particles of the tether and kite
    v_apparent = v_wind_tether - av_vel
    v_app_perp = v_apparent - np.dot(v_apparent, unit_vector) * unit_vector
    # area = norm1 * d_tether * (1 - 0.9 * abs(np.dot(unit_vector, v_apparent) / v_app_norm))
    # half_drag_force = -0.25 * 1.25 * v_app_norm * area * v_apparent
    area = norm1 * d_tether
    half_drag_force = -0.25 * rho * C_D_TETHER * la.norm(v_app_perp) * area * v_app_perp

    forces[p_1] += half_drag_force + spring_force
    forces[p_2] += half_drag_force - spring_force

def innerLoop2_(pos, vel, v_wind_gnd, v_wind_tether, forces, stiffnes_factor, segments, d_tether):
    """
    Calculate the forces, acting on all particles.
    v_wind_tether: out parameter
    forces:        out parameter
    """
    for i in xrange(SPRINGS.shape[0]):
        p_1 = int(SPRINGS[i, 0])  # First point nr.
        p_2 = int(SPRINGS[i, 1])  # Second point nr.
        height = 0.5 * (pos[p_1][2] + pos[p_2][2])
        rho = RHO_0 * math.exp(-height / 8550.0)
        v_wind_tether[0] = calcWindHeight(v_wind_gnd[0], height)
        v_wind_tether[1] = calcWindHeight(v_wind_gnd[1], height)
        v_wind_tether[2] = calcWindHeight(v_wind_gnd[2], height)
        # print "height, v_wind_tether: ", height, v_wind_tether
        calcParticleForces_(pos[p_1], pos[p_2], vel[p_1], vel[p_2], v_wind_tether, SPRINGS[i], forces, \
                           stiffnes_factor, segments, d_tether, rho, i)

def calcAeroForces4_(forces, pos, vel, v_wind, rho, alpha_depower, rel_steering, alpha_zero):
    """
    pos0, pos3, pos4: position of the kite particles P0, P3, and P4
    v2, v3, v4:       velocity of the kite particles P2, P3, and P4
    rho:              air density [kg/m^3]
    rel_depower:      value between  0.0 and  1.0
    rel_steering:     value between -1.0 and +1.0
    """
    pos2, pos3, pos4 = pos[SEGMENTS+2], pos[SEGMENTS+3], pos[SEGMENTS+4]
    v2, v3, v4 = vel[SEGMENTS+2], vel[SEGMENTS+3], vel[SEGMENTS+4]
    va_2, va_3, va_4 = v_wind - v2, v_wind - v3, v_wind - v4
    pos_centre = 0.5 * (pos3 + pos4)
    # print pos_centre
    delta = pos2 - pos_centre
    z = -normalize(delta)
    y = normalize(pos3 - pos4)
    x = np.cross(y, z)
    va_xz2 = va_2 - np.dot(va_2, y) * y
    va_xy3 = va_3 - np.dot(va_3, z) * z
    va_xy4 = va_4 - np.dot(va_4, z) * z

    alpha_2 = (pi - acos(np.dot(normalize(va_xz2), x)) - alpha_depower) * 360.0 / pi + alpha_zero
    alpha_3 = (pi - acos(np.dot(normalize(va_xy3), x)) - rel_steering * KS) * 360.0 / pi + ALPHA_ZTIP
    alpha_4 = (pi - acos(np.dot(normalize(va_xy4), x)) + rel_steering * KS) * 360.0 / pi + ALPHA_ZTIP

    CL2, CD2 = calc_cl(alpha_2), DRAG_CORR * calc_cd(alpha_2)
    CL3, CD3 = calc_cl(alpha_3), DRAG_CORR * calc_cd(alpha_3)
    CL4, CD4 = calc_cl(alpha_4), DRAG_CORR * calc_cd(alpha_4)

    L2 = -0.5 * rho * (la.norm(va_xz2))**2 * AREA * CL2 * normalize(np.cross(va_2, y))
    # print rho, AREA, L2
    L3 = -0.5 * rho * (la.norm(va_xy3))**2 * AREA * REL_SIDE_AREA * CL3 * normalize(np.cross(va_3, z))
    L4 = -0.5 * rho * (la.norm(va_xy4))**2 * AREA * REL_SIDE_AREA * CL4 * normalize(np.cross(z, va_4))
    D2 = -0.5 * K * rho * la.norm(va_2) * AREA * CD2 * va_2
    D3 = -0.5 * K * rho * la.norm(va_3) * AREA * REL_SIDE_AREA * CD3 * va_3
    D4 = -0.5 * K * rho * la.norm(va_4) * AREA * REL_SIDE_AREA * CD4 * va_4
    forces[SEGMENTS + 2] += (L2 + D2)
    forces[SEGMENTS + 3] += (L3 + D3)
    forces[SEGMENTS + 4] += (L4 + D4)

def loop_(scalars, vec3, masses, forces, pos, vel, posd, veld, res0, res1):
    """ Calculate the vector res0 using a vector expression, and calculate res1 using a loop
        that iterates over all tether segments. """
    res0[0] = pos[0]
    res1[0] = vel[0]
    res0[1:NO_PARTICLES] = vel[1:NO_PARTICLES] - posd[1:NO_PARTICLES]
    # Compute the masses and forces
    l_0 = scalars[Length]
    m_tether_particle = MASS * l_0 / L_0
    masses[SEGMENTS] = KCU_MASS + 0.5 * m_tether_particle
    for i in xrange(0, SEGMENTS):
        masses[i] = m_tether_particle
        SPRINGS[i, 2] = scalars[Length] # Current unstressed length
        SPRINGS[i, 3] = scalars[C_spring] / scalars[Stiffnes_factor]
        SPRINGS[i, 4] = scalars[Damping]
    innerLoop2_(pos, vel, vec3[V_wind_gnd], vec3[V_wind_tether], forces, \
                scalars[Stiffnes_factor], int(SEGMENTS), D_TETHER)
    for i in xrange(1, NO_PARTICLES):
        res1[i] = veld[i] - (G_EARTH - forces[i] / masses[i])

def test_calcParticleForces():
    """
    Test the results of the function calcParticleForces of the imported module against the
    locally defined version; raise an assertion error if the test fails.
    """
    vec3 = np.zeros((22, 3))
    pos1 = np.array((1.0, 2.0, 3.0))
    pos2 = np.array((2.0, 3.0, 4.0))
    vel1 = np.array((3.0, 4.0, 5.0))
    vel2 = np.array((4.0, 5.0, 6.0))
    rho = RHO_0
    for i in range(SPRINGS.shape[0]):
        spring = SPRINGS[i]
        # print "spring", spring
        forces = (np.zeros((SEGMENTS + PARTICLES.shape[0] - 1, 3), order='C'))
        stiffnes_factor = 0.5
        v_wind_tether = np.array((8.0, 0.1, 0.0))
        calcParticleForces_(pos1, pos2, vel1, vel2, v_wind_tether, spring, forces, stiffnes_factor, SEGMENTS, \
                            D_TETHER, rho, 1)
        forces1 = np.array(forces)
        # print forces1
        # print
        forces.fill(0.0)
        # print "spring", spring
        calcParticleForces(vec3, pos1, pos2, vel1, vel2, v_wind_tether, spring, forces, stiffnes_factor, \
                           SEGMENTS, D_TETHER, rho, 1)
        # print forces
        # print
        assert np.linalg.norm(forces - forces1) <= 1e-10
    print "Finished test_calcParticleForces()"

def test_innerLoop():
    """
    Test the results of the function innerLoop2 of the imported module against the
    locally defined version; raise an assertion error if the test fails.
    """
    vec3 = np.zeros((VEC3_SIZE, 3))
    pos, vel = init()
    # print 'pos.shape: ', pos.shape
    # print SEGMENTS + KITE_PARTICLES + 1
    # print "pos.shape", pos.shape
    v_wind = np.array((7.0, 0.1, 0.0))
    v_wind_tether = np.zeros(3)
    stiffnes_factor = 0.5
    forces = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1, 3))
    innerLoop2_(pos, vel, v_wind, v_wind_tether, forces, stiffnes_factor, SEGMENTS, D_TETHER)
    forces1 = np.array(forces)
    # print 'forces1 =', forces1
    forces.fill(0.0)
    scalars = np.zeros(SCALARS_SIZE)
    scalars[Alpha] = 1.0/7.0
    innerLoop2(vec3, scalars, pos, vel, v_wind, v_wind_tether, SPRINGS, forces, stiffnes_factor, SEGMENTS, D_TETHER)
    # print 'forces =', forces
    assert la.norm(forces - forces1) <= 1e-8
    print "Finished test_innerLoop()"

def init2():
    vec3 = np.zeros((VEC3_SIZE, 3))
    scalars = np.zeros(SCALARS_SIZE)
    pos, vel = init()
    posd = vel.copy()
    veld = np.zeros_like(vel)
    # print 'pos.shape: ', pos.shape
    # print SEGMENTS + KITE_PARTICLES + 1
    # print "pos.shape", pos.shape
    vec3[V_wind_gnd][:] = np.array((7.0, 0.1, 0.0))
    vec3[V_wind_tether][:] = np.zeros(3)
    scalars[Stiffnes_factor] = 0.5
    scalars[Alpha] = 1.0/7.0
    length = 152.0
    scalars[Length] = length / SEGMENTS
    scalars[Damping]  = DAMPING  * L_0 / scalars[Length]
    scalars[C_spring] = C_SPRING * L_0 / scalars[Length]
    scalars[D_tether] = D_TETHER
    forces = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1, 3))
    res = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1) * 6).reshape((2, -1, 3))
    if WINCH_MODEL:
        res = np.append(res, 0.0) # res_length
        res = np.append(res, 0.0) # res_v_reel_out
    res0, res1 = res[0:-2].reshape((2, -1, 3))[0], res[0:-2].reshape((2, -1, 3))[1]
    return vec3, scalars, pos, vel, posd, veld, forces, res0, res1


def test_loop():
    vec3, scalars, pos, vel, posd, veld, forces, res0, res1 = init2()

    loop_(scalars, vec3, MASSES, forces, pos, vel, posd, veld, res0, res1)

    res0_ = res0.copy()
    res1_ = res1.copy()
    SPRINGS_ = SPRINGS.copy()
    scalars_ = scalars.copy()
    forces_ = forces.copy()

    vec3, scalars, pos, vel, posd, veld, forces, res0, res1 = init2()

    loop(scalars, vec3, MASSES, SPRINGS, forces, pos, vel, posd, veld, res0, res1)

    # print res1
    assert la.norm(res0 - res0_) <= 1e-8
    assert la.norm(SPRINGS - SPRINGS_) <= 1e-8
    assert la.norm(scalars - scalars_) <= 1e-8

    assert la.norm(forces - forces_) <= 1e-8
    assert la.norm(res1 - res1_) <= 1e-8
    print "Finished test_loop()"

def test_calcAeroForces4():
    """
    Test the results of the function calcAeroForces4 of the imported module against the
    locally defined version; raise an assertion error if the test fails.
    """
    scalars = np.zeros(SCALARS_SIZE)
    vec3 = np.zeros((VEC3_SIZE, 3))
    pos, vel = init()
    rho = 1.25
    v_wind = np.array((8.0, 0.2, 0.0))
    vec3[V_wind][:] = v_wind
    alpha_depower = 0.1
    rel_steering = -0.1
    scalars[Alpha_zero] = 5.0
    forces = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1, 3))
    forces.fill(0.0)
    calcAeroForces4_(forces, pos, vel, v_wind, rho, alpha_depower, rel_steering, scalars[Alpha_zero])
    # print forces
    forces1 = np.array(forces)

    forces.fill(0.0)

    calcAeroForces4(scalars, vec3, forces, pos, vel, rho, alpha_depower, rel_steering)
    # print forces
    assert la.norm(forces - forces1) <= 1e-8
    print "Finished test_calcAeroForces4()"

if __name__ == "__main__":
    np.set_printoptions(precision=4, suppress=True)
    test_calcParticleForces()
    test_innerLoop()
    test_calcAeroForces4()
    # test_loop()





