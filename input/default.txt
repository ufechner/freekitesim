% This file contains a list of connections of bridlepoints. They are
% defined in pairs [point_n point_k L_0]. The ground station
% is indicated by a 0. L_0 is only recognised for the tether.
% First point, second point, unstressed length. Numbers seperated by a space.

0 1 150.0

