/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef DISPLAYBACKGROUND_H
#define DISPLAYBACKGROUND_H

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include <QDebug>
#include <cmath>

// Responsibility: Drawing of the background items that display instrument background
class DisplayBackground : public QGraphicsItem
{
public:
    // constructor
    DisplayBackground();

    // functions
    QRectF boundingRect() const;        // Implementation of the pure virtual public functions of a QGraphicsItem
                                        // returns an estimate of the area painted by the item. necessary because
                                        // it is called by the scene where the item is placed.

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
                                        // Implementation of a pure virtual public functions of a QGraphicsItem
                                        // paints subitems into the background item

    // methods (setters)
    void computeLongitude();            // Compute the longitude curves that make up the
                                        // background of the instrument
    void computeLatitude();             // Compute the latitude curves that make up the
                                        // background of the instrument

private:
    // attributes
    QList<QPolygonF> longitudePolygons; // Qlist of QPolygonF vectors of QPointF objects of the coordinates for
                                        // the longitudinal curves of the instruments background
    QList<QPolygonF> latitudePolygons;  // Qlist of QPolygonF vectors pf QPointF objects of the coordinates for
                                        // the latitude curves of the instruments background

};

#endif // DISPLAYBACKGROUND_H
