/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "displaykite.h"
#include "QsLog.h"

DisplayKite::DisplayKite()
{
    QLOG_DEBUG() << "Created DisplayKite...";
    m_kitePoint    = QPointF(0.0,0.0);
    m_kiteTrail.clear();
    m_headingPoint = QPointF(0.0,0.0);
    m_coursePoint  = QPointF(0.0,0.0);
    m_headingAngle = 0.0;
    m_courseAngle  = 0.0;
}

QRectF DisplayKite::boundingRect() const
{
    return QRectF(-280, -150, 560, 300);
}

void DisplayKite::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipRect( option->exposedRect );
    (void) widget;
    painter->translate(0,125);
    QPen bluePen(Qt::blue, 10, Qt::SolidLine,Qt::RoundCap);
    QBrush brush(Qt::white);
    painter->setPen(bluePen);
    painter->setBrush(brush);
    painter->drawPoint(m_kitePoint);

    QPen smallBluePen(Qt::blue, 5, Qt::SolidLine,Qt::RoundCap);
    painter->setPen(smallBluePen);
    for(int i = 0 ; i < m_kiteTrail.size(); i++) {
        painter->drawPoint(m_kiteTrail[i]);
    }

    QPen smallerBluePen(Qt::blue, 2, Qt::SolidLine,Qt::RoundCap);
    painter->setPen(smallerBluePen);
    QLineF headingLine;
    headingLine.setP1(m_kitePoint);
    headingLine.setP2(m_headingPoint);
    painter->drawLine(headingLine);
    painter->drawLine(-275, -240, -265, -240);

    QPen smallGreenPen(Qt::green, 2, Qt::SolidLine,Qt::RoundCap);
    painter->setPen(smallGreenPen);
    QLineF courseLine;
    courseLine.setP1(m_kitePoint);
    courseLine.setP2(m_coursePoint);
    painter->drawLine(courseLine);
    painter->drawLine(-275, -260, -265, -260);

    QFont backgroundFont("Helvetica [Cronyx]", 8);
    painter->setFont(backgroundFont);

    QPen blackPen(Qt::black, 2 , Qt::SolidLine,Qt::RoundCap);
    painter->setPen(blackPen);
    painter->drawText(-195, -250, 60, 20, Qt::AlignLeft,
                      QString::number((m_headingAngle * 180 / M_PI), 'f', 1) + QString(" deg"));
    painter->drawText(-195, -270, 60, 20, Qt::AlignLeft,
                      QString::number((m_courseAngle * 180 / M_PI), 'f', 1) + QString(" deg"));

    QLOG_DEBUG() << "DisplayKite: Painted...";
}

void DisplayKite::clear()
{
    double trailSize = m_trailSize;
    setKiteTrailSize(0);
    setKiteTrailSize(trailSize);
}

void DisplayKite::setKiteStateParameters(QPointF kitePoint, QPointF headingPoint, QPointF coursePoint,
                                         double headingAngle, double courseAngle)
{
    prepareGeometryChange();
    this->m_kitePoint    = kitePoint;
    this->m_kiteTrail.prepend(this->m_kitePoint);
    while(this->m_kiteTrail.size() > this->m_trailSize) this->m_kiteTrail.removeLast();
    this->m_headingPoint = headingPoint;
    this->m_coursePoint  = coursePoint;
    this->m_headingAngle = headingAngle;
    this->m_courseAngle  = courseAngle;
    QLOG_DEBUG() << "DisplayKite: Kite parameters updated...";
}

void DisplayKite::setKiteTrailSize(int trailSize)
{
    prepareGeometryChange();
    this->m_trailSize =  trailSize;
    while(this->m_kiteTrail.size() > this->m_trailSize) this->m_kiteTrail.removeLast();
    QLOG_DEBUG() << "DisplayKite: Kite trail size updated...";
}
