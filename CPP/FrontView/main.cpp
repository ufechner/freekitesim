#include <QtGui/QApplication>
#include "frontview.h"
#include "QsLog.h"
#include "QsLogDest.h"

int main(int argc, char *argv[])
{
    QApplication::setGraphicsSystem("raster");
    QApplication a(argc, argv);

    // init the logging mechanism
    QsLogging::Logger& logger = QsLogging::Logger::instance();
    // use the logging lever TraceLevel (show everything)
    logger.setLoggingLevel(QsLogging::InfoLevel);
    QsLogging::DestinationPtr debugDestination(QsLogging::DestinationFactory::MakeDebugOutputDestination() );
    logger.addDestination(debugDestination.get());

    FrontView w;
    w.show();

    return a.exec();
}
