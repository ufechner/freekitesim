Viewer for the kite trajectory as seen from the front.

It uses a projection of the position of the kite on the "small earth",
a half sphere with a radius of one around the ground-station.

Converting this code to Python would make the simulator easier to install
and to maintain.

This code was originally developed by Filip Saad.

Uwe Fechner, TU Delft, 12.01.2014
