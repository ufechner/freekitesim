#-------------------------------------------------
#
# Project created by QtCreator 2012-01-14T18:01:33
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = ProtoBuf
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# BEGIN section protobuf
PROTOS = ../../00AssetLib/ProtoBuf/asset.pod.proto \
         ../../00AssetLib/ProtoBuf/asset.winch.proto \
         ../../00AssetLib/ProtoBuf/asset.system.proto

include(../../00AssetLib/ProtoBuf/protobuf.pri)

LIBS += /usr/local/lib/libprotobuf.so
# END section protobuf

SOURCES += main.cpp

OTHER_FILES += \
    Readme.txt \
    asset.winch.proto \
    asset.system.proto \
    asset.pod.proto
