/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "utils.h"
#include <cmath>
#include <sys/time.h>
#include <QThread>
#include <QDebug>
#include <QHostInfo>

class SleeperThread : public QThread
{
public:
    static void usleep(unsigned long usecs)
    {
        QThread::usleep(usecs);
    }
};

double Utils::timeInSec()
{
    struct timeval timeOfDay;
    gettimeofday(&timeOfDay,0);
    double highResolutionTime = timeOfDay.tv_sec + (timeOfDay.tv_usec * 1.0e-6);
    return highResolutionTime;
}

QDateTime Utils::timeObject(double time)
{
    double time_ms = time * 1000;
    return QDateTime::fromMSecsSinceEpoch(time_ms);
}

 void Utils::usleep(unsigned long usecs){
     SleeperThread::usleep(usecs);
 }

 double Utils::exp125(int linearSetting)
 {
     int decade = linearSetting / 3;     
     int range = linearSetting % 3;
     if (range < 0){
         range+=3;
         decade-=1;
     }
     //qDebug() << decade << range;
     double result=1;
     if (range==1) result=2;
     if (range==2) result=5;
     if (decade > 0){
         while (decade > 0){
             decade--;
             result*=10;
         }
     }
     if (decade < 0){
         while (decade < 0){
             decade++;
             result/=10;
         }
     }
     return (result);
 }

 int Utils::log125(double value)
 {
     int decade = 0;
     int range = 1;
     while (value > 10.0){
         decade+=1;
         value/=10;
     }
     if (value <= 1.5){
         range = 0;
     } else if (value <= 3.5){
         range = 1;
     } else if (value <= 7.5){
         range = 2;
     } else {
         range = 3;
     }
     //qDebug() << "log125: " << value << decade << range;
     return (decade*3+range);
 }

 double Utils::round(double value)
 {
     return floor(value * 100.0 + 0.5) / 100.0;
 }

 bool Utils::isEqual(double value1, double value2)
 {
     if (value2 == 0) {
         if (fabs(value1) < 1e-9) {
             return true;
         } else {
             return false;
         }
     }
     if (value1 == 0) {
         if (fabs(value2) < 1e-9) {
             return true;
         } else {
             return false;
         }
     }
     double quotient = fabs(value1 / value2);
     if (quotient < 1.0) {
         quotient = 1.0 / quotient;
     }
     return ((quotient - 1.0) < 0.001);
 }

 bool Utils::isAlmostEqual(double value1, double value2, int digits)
 {
     double precision = pow(1, -digits);
     if (value2 == 0) {
         if (fabs(value1) < precision) {
             return true;
         } else {
             return false;
         }
     }
     if (value1 == 0) {
         if (fabs(value2) < precision) {
             return true;
         } else {
             return false;
         }
     }
     double quotient = fabs(value1 / value2);
     if (quotient < 1.0) {
         quotient = 1.0 / quotient;
     }
     return ((quotient - 1.0) < precision);
 }

 int Utils::yOffset()
 {
     if (m_hostname == "uwe-ssd") {
         return (8);
     } else if (m_hostname == "ufechner-hp.localnet") {
         return (24);
     } else if (m_hostname == "dell-laptop.localnet") {
         return (8);
     } else {
         return (0);
     }
 }

 int Utils::xOffset()
 {
     if (m_hostname == "uwe-ssd") {
         return (9);
     } else if (m_hostname == "ufechner-hp.localnet") {
         return (4);
     } else if (m_hostname == "dell-laptop.localnet") {
         return (9);
     } else {
         return (0);
     }
 }

 Utils::Utils()
 {
     // determine hostname
     m_hostname = QHostInfo::localHostName();
 }
