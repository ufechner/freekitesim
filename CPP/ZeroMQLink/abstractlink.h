/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef ABSTRACTLINK_H
#define ABSTRACTLINK_H

#include <QObject>

// interface for 1:1 or 1:n links, that can be used to exchange binary messages
// between programs; can be used as base class for UDPLink, SerialLink and ZeroMQLink
class AbstractLink : public QObject
{
    Q_OBJECT
public:
    explicit AbstractLink(QObject *parent = 0);
    //~AbstractLink();
    // send the given, binary encoded message
    virtual void sendMessage(QByteArray message, int msg_type = 0) = 0;
    // bind the output (ZeroMQ) or input (UDP) to the given port
    // returns zero on success
    // ZeroMQ: "tcp://*:5561"
    // UDP: "dell-laptop:5824" localhost becomes UDP server and receives messages
    virtual int bind(QString connection) = 0;
    // connect the input (ZeroMQ) or output (UDP) to the given server and port
    // can be called multiple times, to subscribe to different servers and/or ports (only ZeroMQ)
    // returns zero on success
    // UDP: "dell-laptop:5825" localhost becomes UDP client and sends messages to dell-laptop
    // ZeroMQ: "tcp://localhost:5560" connect to the clock server, that is running on localhost
    virtual int connect_link(QString connection) = 0;
    // subscribe to messages with the given message type
    // can be called multiple times, to subscribe to different messages
    virtual void subscribe(int msg_type) = 0;
    // fetch the last message, that was received; should be called, after the event messageReceived was received
    virtual QByteArray getLastMessage(int msg_type) = 0;

public slots:
    // should be called at least every 10 ms; currently only needed for serial links
    virtual void onTimer(void) = 0;

signals:
    // this signal is emitted, when a new input message has been detected in the onTimer slot
    // void messageReceived(int msg_type);

private:
};


#endif // ABSTRACTLINK_H
